//Get Afrane Scene
var sceneEl = document.querySelector('a-scene');

//Create a Box with dimensions,physics and shadows
AFRAME.registerComponent('woman', {
  init: function () {
      this.el.addEventListener('trackpadtouchend', event => alert('tapped!'));
      this.el.setAttribute('obj-model', {
          obj: this.querySelector('a-entity#obj-model')
      });
      this.el.setAttribute('position', {
        x: -2,
        y: 0,
        z: -10
      });
      this.el.setAttribute('dynamic-body', {});
      this.el.setAttribute('shadow', {receive: true});
  }
});